iphone-friendly-cmake
---------------------

* CMake now supports :ref:`Cross Compiling for iOS, tvOS, or watchOS`
  using simple toolchain files.
